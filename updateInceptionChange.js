const axios = require('axios');
const admin = require('firebase-admin');
const FieldValue = require("firebase-admin").firestore.FieldValue;
const serviceAccount = require('./service_account/hodler-list-firebase-adminsdk-phjjp-bd369ace70.json');
admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://hodler-list.firebaseio.com"
});

/* <CONSTANTS> */
const PriceBase = 'https://min-api.cryptocompare.com/data/price';
/* </CONSTANTS> */

/* <FUNCTIONS> */
async function getCurrentPrice(coinId) {
  const response = await axios.get(PriceBase, {
    params: {
      fsym: coinId,
      tsyms: 'USD',
    },
  });

  if (response.data.Response === 'Error') {
    throw new Error(response.data.Message);
  }

  const price = parseFloat(response.data.USD);
  return price;
};

async function updatePortfoliosInBatch(valuesArr, batchSize) {
  if (valuesArr.length) {
    const batchArr = valuesArr.splice(0, batchSize);
    const batch = admin.firestore().batch();
    batchArr.forEach((v) => {
      const portfolioRef = v.portfolioRef;
      const changeSinceInception = v.changeSinceInception;
      const coins = v.coins
      batch.update(portfolioRef, { changeSinceInception, coins });
    });
    await batch.commit();

    updatePortfoliosInBatch(valuesArr, batchSize);
  }
};
/* </FUNCTIONS> */


async function updatePortfolios(batchSize)  {
  // Iterate through each portfolio and update portfolio value
  const portfoliosValuesArr = [];

  try {
    const portfoliosRef = admin.firestore().collection('portfolios');
    const snapshot = await portfoliosRef.get();
    await Promise.all(snapshot.docs.map(async (doc) => {
      const singlePortfolioRef = doc.ref;

      // Get coins in portfolio and calculate the current portfolio value
      const coins = doc.data().coins;
      let currentPortfolioValue = 0;
      await Promise.all(coins.map(async (c) => {
        const coinId = c.id;
        const amount = parseFloat(c.amount);
        try {
          const currentPrice = await getCurrentPrice(coinId);
          currentPortfolioValue += currentPrice * amount;

          // Save current coin price as a backup
          c.lastPrice = currentPrice;
        } catch(e) {
          console.log(`Error while getting coin current price: '${e.message}'\nUsing the last saved price instead`);

          // Use last saved current price as a fallback
          currentPortfolioValue += c.lastPrice * amount;
        }
      }));

      // Calculate % change since the inception
      const inceptionValue = parseFloat(doc.data().inceptionValue);
      let changeSinceInception = 0;
      if (currentPortfolioValue > inceptionValue) {
        changeSinceInception = ((currentPortfolioValue - inceptionValue) / inceptionValue) * 100;
      } else {
        changeSinceInception = -1 * ((inceptionValue - currentPortfolioValue) / inceptionValue) * 100;
      }

      portfoliosValuesArr.push({
        portfolioRef: singlePortfolioRef,
        changeSinceInception,
        coins,
      });
    }));

    await updatePortfoliosInBatch(portfoliosValuesArr, batchSize);
  }
  catch(error) {
    console.log('Error while updating inception change', error);
  }
};

console.log('Updating inception changes...');
const batchSize = 100;
updatePortfolios(batchSize);
