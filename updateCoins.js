const axios = require('axios');
const admin = require('firebase-admin');
const serviceAccount = require('./service_account/hodler-list-firebase-adminsdk-phjjp-bd369ace70.json');
admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://hodler-list.firebaseio.com"
});

console.log('Updating coins...');
const API_BASE = 'https://api.coinmarketcap.com/v1/ticker/';
const endPoint = 'https://www.cryptocompare.com/api/data/coinlist/';
axios.get(endPoint)
  .then((getResponse) => {
    const data = getResponse.data.Data;

    for (const key in data) {
      const coinObj = data[key];

      const id = coinObj.Symbol;
      const name = coinObj.CoinName;
      const symbol = coinObj.Symbol;

      admin.firestore().collection('coins').doc(id).set({
        id,
        name,
        symbol
      })
        .then(() => {
          console.log(`\t ✅  Coin '${id}' written into DB`);
        })
        .catch((err) => {
          console.log(`\t ❌  Error while writing coin '${id}'`, err);
        });
    }
  })
  .catch((err) => {
    console.error(`❌ Error while fetching coinmarketcap data`, err);
  });
